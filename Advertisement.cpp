// Working group: 2.2.2 (A.Solovev, D.Tochilkin, I.Trofimov)
// Author: Tochilkin Dmitry, Andrey Solovev
// Group: 272(2)
// Date: 9 Feb 2014

#include "Advertisement.h"

Advertisement:: Advertisement() : number(0), quantity(0), title(""), seller_email(""),
    body(""), start(Date()), close(Date()) {}

Advertisement:: Advertisement(string title, string seller_email, string body,
                              Date start, Date close, int quantity) :
    number(0), quantity(quantity), title(title), seller_email(seller_email),
    body(body), start(start), close(close) {}

Advertisement:: Advertisement(const Advertisement& a) :
    number(a.number), quantity(a.quantity), title(a.title), seller_email(a.seller_email),
    body(a.body), start(a.start), close(a.close) {}


void Advertisement :: setStart (const Date& value) {
    start = value;
}

void Advertisement :: setClose (const Date& value) {
    close = value;
}

void Advertisement :: setTitle (string value) {
    title = value;
}

void Advertisement :: setBody (string value) {
    body = value;
}

void Advertisement :: setEmail (string value) {
    seller_email = value;
}

void Advertisement :: setNumber (int value) {
    number = value;
}

void Advertisement :: setQuantity (int value) {
    quantity = value;
}

Date Advertisement :: getStart () const {
    return start;
}

Date Advertisement :: getClose () const {
    return close;
}

string Advertisement :: getTitle () const {
    return title;
}

string Advertisement :: getBody () const {
    return body;
}

string Advertisement :: getEmail () const {
    return seller_email;
}

int Advertisement :: getNumber () const {
    return number;
}

int Advertisement :: getQuantity () const {
    return quantity;
}

bool Advertisement :: operator== (const Advertisement& a) const {
    return (a.getNumber() == number);
}

istream& operator>>(istream& stream, Advertisement& a) {
    string temp;
    Date start, close;

    getline(stream, temp);
    temp.erase(0, temp.find_first_not_of(' '));
    a.setTitle(temp);

    getline(stream, temp);
    temp.erase(0, temp.find_first_not_of(' '));
    a.setEmail(temp);

    getline(stream, temp);
    a.setQuantity(std::stoi(temp));

    stream >> start >> close;
    a.setStart(start);
    a.setClose(close);

    getline(stream, temp);
    temp.erase(0, temp.find_first_not_of(' '));
    a.setBody(temp);

    return stream;
}

priority_queue<Bid>& Advertisement::getBids() {
	return bids;
}

vector<Bid> Advertisement::getTopDutchBids() const {

	int availableQuantity = this->quantity;
	priority_queue<Bid> bidQueue = bids;

	vector<Bid> bestBids;
	while (bidQueue.size() != 0 && availableQuantity > 0)
	{
		availableQuantity -= bidQueue.top().getQuantity();
		bestBids.push_back(bidQueue.top());
		bidQueue.pop();

		// if (bestBids.size() == 5) break;
	}

	return bestBids;

}