//��������� ������ 2.2.2
#include "bidhistory.h"     

void displayBidHistory(ostringstream &oss, Advertisement* ad) {     
	priority_queue<Bid> bids = ad->getBids();   
	int bidsNumber = bids.size();  
	int quantity = ad->getQuantity();   
	Client *seller = users[ad->getEmail()];   
  
	oss << "Posted by: " << seller->getFname() << " " 
	 << seller->getLname() << "</a><br>" << endl;   
	oss << "Post date: " << ad->getStart() << "<br>" << endl;   
	oss << "Close date: " << ad->getClose() << "<br>" << endl;   
	oss << "Quantity: " << quantity << "<br><br>" << endl;   

	if(bidsNumber == 0){   
		oss << "No bids on this advertisement.<br>" << endl;  
		return;   
	}   

	oss<<bidsNumber<<" bid(s) have found.<br>\n";  

	if(quantity == 1){	 
		Bid current = bids.top();   
		oss<<"The highest bid is $"<<current.getAmount()<<
			" (bet by "<<current.getEmail()<<")<br>\n";   
		return;   
	}   

	oss << "Winning bids: " << "<br>\n<br>\n";   

	int total = 0;   
	for (int i = 0; i < bidsNumber && total <= quantity; i++) {   
		Bid cur = bids.top();   
		bids.pop();   
		int itemNumber = cur.getQuantity();   
		total += itemNumber;   

		if(total > quantity)  
			itemNumber -= (total - quantity);   
  
		oss<<"<br>\t" << i + 1 << ".  "; 
		oss << cur.getEmail() << " makes bid $"<<
			cur.getAmount() << " and won "   
			<< itemNumber << " of "   
			<< cur.getQuantity() << " items.<br>\n";   
	}   

	int unbid = (quantity - total) < 0 ? 0 : (quantity - total);   
	oss<<"<br>\n"<<unbid<<" item(s) remain unbid.<br>\n";    
} 