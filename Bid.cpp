// Working group: 2.2.2 (A.Solovev, D.Tochilkin, I.Trofimov)
// Authors: Andrey Solovev
// Group: 272(2)
// Date: 9 Apr 2014

#ifndef BID_CPP
#define BID_CPP

#include "Bid.h"

// Trim spaces
string trimSpace(string s) {
    return s.erase(s.find_last_not_of(" \t") + 1)
           .erase(0, s.erase(s.find_last_not_of(" \t") + 1)
                  .find_first_not_of(" \t"));
}

Bid::Bid(void)
    : email(""), amount(0), quantity(0) {
};

Bid::Bid(const Bid& b)
    : email(b.getEmail()), amount(b.getAmount()),
      quantity(b.getQuantity()), date(b.getDate()) {
};

Bid::Bid(string email, float amount, int quantity, Date date):
    email(email), amount(amount), quantity(quantity), date(date) {
};

string Bid::getEmail() const {
    return email;
}

float Bid::getAmount() const {
    return amount;
}

int Bid::getQuantity() const {
    return quantity;
}

Date Bid::getDate() const {
    return date;
}

void Bid::setEmail(const string& email) {
    this->email = email;
}

void Bid::setAmount(const float& amount) {
    this->amount = amount;
}

void Bid::setQuantity(const int& quantity) {
    this->quantity = quantity;
}

void Bid::setDate(const Date& date) {
    this->date = date;
}

bool Bid::operator< (const Bid& rhs) const {
    return (amount < rhs.amount);
}

bool Bid::operator== (const Bid& rhs) const {
    return (rhs.amount == amount);
}

istream& operator>>(istream& stream, Bid& b) {
    string line;

    getline(stream, line);
    b.setEmail(trimSpace(line));

    getline(stream, line);
    b.setAmount(stof(trimSpace(line)));

    getline(stream, line);
    b.setQuantity(stoi(trimSpace(line)));

    Date getdate;
    stream >> getdate;
    b.setDate(getdate);

    return stream;
}

#endif