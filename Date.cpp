// Working group: 2.2.2 (A.Solovev, D.Tochilkin, I.Trofimov)
// Author: Andrey Solovev
// Group: 272(2)
// Date: 8 Feb 2014

#include <iostream>
#include "Date.h"
#include <iomanip>
#include <string>

using namespace std;

// Default constructor
Date::Date(void):
    month(0),
    day(0),
    year(0),
    hour(0),
    minute(0),
    second(0) {
};

// Six parameter constructor
Date::Date(int month, int day, int year, int hour, int minute, int second):
    month(month),
    day(day),
    year(year),
    hour(hour),
    minute(minute),
    second(second) {
};

// Month accessor
int Date::getMonth(void) const {
    return month;
}

// Day accessor
int Date::getDay(void) const {
    return day;
}

// Year accessor
int Date::getYear(void) const {
    return year;
}

// Hour accessor
int Date::getHour(void) const {
    return hour;
}

// Minute accessor
int Date::getMinute(void) const {
    return minute;
}

// Second accessor
int Date::getSecond(void) const {
    return second;
}

// Month mutator
void Date::setMonth(int& setMonth) {
    month = setMonth;
}

// Day mutator
void Date::setDay(int& setDay) {
    day = setDay;
}

// Year mutator
void Date::setYear(int& setYear) {
    year = setYear;
}

// Hour mutator
void Date::setHour(int& setHour) {
    hour = setHour;
}

// Minute mutator
void Date::setMinute(int& setMinute) {
    minute = setMinute;
}

// Second mutator
void Date::setSecond(int& setSecond) {
    second = setSecond;
}

// Compares Date objects for equality
bool Date::operator== (const Date& rhs) {
    if (month == rhs.getMonth()
            && day == rhs.getDay()
            && year == rhs.getYear()) {

        return (hour == rhs.getHour()
                && minute == rhs.getMinute()
                && second == rhs.getSecond());
    }

    return false;
}

// Compares Date objects, returns true if the invoking Date object is less
// than the other Date object
bool Date::operator< (const Date& rhs) {
    if (this->year <= rhs.getYear()) {
        if (this->year < rhs.getYear()) {
            return true;
        }

        if (this->month <= rhs.getMonth()) {
            if (this->month < rhs.getMonth()) {
                return true;
            }

            if (this->day <= rhs.getDay()) {
                if (this->day < rhs.getDay()) {
                    return true;
                }

                if (this->hour <= rhs.getHour()) {
                    if (this->hour < rhs.getHour()) {
                        return true;
                    }

                    if (this->minute <= rhs.getMinute()) {
                        if (this->minute < rhs.getMinute()) {
                            return true;
                        }

                        if (this->second < rhs.getSecond()) {
                            return true;
                        }
                    }
                }
            }
        }
    }

    return false;
}

// Outputs a Date object to an output stream in the form "mm/dd/yyyy hh:nn:ss"
ostream& operator<<(ostream& stream, const Date& date) {
    stream << setfill('0') << setw(2) << date.getMonth() << '/';
    stream << setfill('0') << setw(2) << date.getDay() << '/';
    stream << setfill('0') << setw(4) << date.getYear() << ' ';

    stream << setfill('0') << setw(2) << date.getHour() << ':';
    stream << setfill('0') << setw(2) << date.getMinute() << ':';
    stream << setfill('0') << setw(2) << date.getSecond();

    return stream;
}

// This method reads a Date object from an input stream.
istream& operator>>(istream& stream, Date& date) {
    string line, sval;
    getline(stream, line);

    sval = line.substr(0, line.find_first_of("/"));
    int value = atoi(sval.c_str());
    date.setMonth(value);
    line = line.substr(line.find_first_of("/") + 1);

    sval = line.substr(0, line.find_first_of("/"));
    value = atoi(sval.c_str());
    date.setDay(value);
    line = line.substr(line.find_first_of("/") + 1);

    sval = line.substr(0, line.find_first_of(" "));
    value = atoi(sval.c_str());
    date.setYear(value);
    line = line.substr(line.find_first_of(" ") + 1);

    sval = line.substr(0, line.find_first_of(":"));
    value = atoi(sval.c_str());
    date.setHour(value);
    line = line.substr(line.find_first_of(":") + 1);

    sval = line.substr(0, line.find_first_of(":"));
    value = atoi(sval.c_str());
    date.setMinute(value);
    line = line.substr(line.find_first_of(":") + 1);

    value = atoi(line.c_str());
    date.setSecond(value);

    return stream;
}
