/**
 * \file    Listing.cpp
 * \author  Ilya Trofimov
 * \email   ilyatrofimov@outlook.com
 * \date    2014-05-28 08:51:58
 *
 * \modifiedby   Ilya Trofimov
 * \modifiedtime 2014-06-11 15:58:19
 */


#include "Listing.h"

/**
 * Several compare methods for implementing sort method
 */
bool compareEmail(Advertisement* first, Advertisement* second) {
    return first->getEmail() < second->getEmail();
}

bool compareStartDate(Advertisement* first, Advertisement* second) {
    return first->getStart() < second->getStart();
}

bool compareCloseDate(Advertisement* first, Advertisement* second) {
    return first->getClose() < second->getClose();
}

bool compareQuantity(Advertisement* first, Advertisement* second) {
    return first->getQuantity() < second->getQuantity();
}

bool compareHighest(Advertisement* first, Advertisement* second) {
    return (first->getBids().size() == 0 ? 0 : first->getBids().top().getAmount()) >
           (second->getBids().size() == 0 ? 0 : second->getBids().top().getAmount());
}

bool compareLowest(Advertisement* first, Advertisement* second) {
    return (first->getBids().size() == 0 ? 0 : first->getBids().top().getAmount()) <
           (second->getBids().size() == 0 ? 0 : second->getBids().top().getAmount());
}

/**
 * Returns the Advertisement pointer whose number equals the parameter number
 */
Advertisement* Listing::operator[](const int& number) {
    for (iterator it = begin(); it != end(); ++it) {
        if ((*it)->getNumber() == number) {
            return *it;
        }
    }

    return NULL;
}

/**
 * Adds the Advertisement pointer given by the parameter to the vector objects.
 */
void Listing::add(Advertisement* ptr) {
    if (ptr != NULL) {
        objects.push_back(ptr);
    }
}

/**
 * Returns an iterator to the first Advertisement* in vector objects.
 */
Listing::iterator Listing::begin() {
    return objects.begin();
}

/**
 * Returns an iterator to the last Advertisement* in vector objects.
 */
Listing::iterator Listing::end() {
    return objects.end();
}

/**
 * Returns a copy of the invoking Listing object sorted by the field name
 * given in the parameter.
 */
Listing Listing::sort(string field) {
    Listing out;
    out.objects.resize(objects.size());
    std::copy(begin(), end(), out.begin());

    if (field == "email") {
        std::sort(out.begin(), out.end(), compareEmail);
    } else if (field == "start") {
        std::sort(out.begin(), out.end(), compareStartDate);
    } else if (field == "close") {
        std::sort(out.begin(), out.end(), compareCloseDate);
    } else if (field == "quantity") {
        std::sort(out.begin(), out.end(), compareQuantity);
    } else if (field == "highest") {
        std::sort(out.begin(), out.end(), compareHighest);
    } else if (field == "lowest") {
        std::sort(out.begin(), out.end(), compareLowest);
    }

    return out;
}

/**
 * Returns a new instance of a Listing object that contains only those
 * advertisements whose name or description contains the string given by
 * the parameter keyword.
 */
Listing Listing::filter(string keyword) {
    if (keyword == "") {
        return *this;
    }

    Listing res;
    for_each(begin(), end(), [keyword, &res](Advertisement * i) {
        if ((i->getTitle()).find(keyword)
                != i->getTitle().npos
                || (i->getBody()).find(keyword)
                != i->getBody().npos) {
            res.add(i);
        }
    });

    return res;
}
