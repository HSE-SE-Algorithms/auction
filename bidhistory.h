#ifndef BIDHISTORY_H
#define BIDHISTORY_H

#include <sstream>

#include "main.h"
#include "Client.h"
#include "Advertisement.h"
#include "Listing.h"
#include "Group.h"

using namespace std;

void displayBidHistory(ostringstream &oss, Advertisement* ad);

#endif
