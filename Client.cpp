// Working group: 2.2.2 (A.Solovev, D.Tochilkin, I.Trofimov)
// Author: Ilya Trofimov
// Group: 272(2)
// Date: 6 Feb 2014
// Last Edited: 13 May 2014

#include "Client.h"


// Default constructor
Client::Client(void) : fname(""), lname(""), email(""), passwd("") {}

// Copy constructor
Client::Client(Client const& c)
    : fname(c.fname), lname(c.lname), email(c.email), passwd(c.passwd) {}

// Four parameter constructor
Client::Client(string& fname, string& lname, string& email, string& passwd)
    : fname(fname), lname(lname), email(email), passwd(passwd) {}


// First name accessor
string Client::getFname() const {
    return fname;
}

// First name mutator
void Client::setFname(const string& value) {
    fname = value;
}

// Last name accessor
string Client::getLname() const {
    return lname;
}

// Last name mutator
void Client::setLname(const string& value) {
    lname = value;
}

// Email accessor
string Client::getEmail() const {
    return email;
}

// Email mutator
void Client::setEmail(const string& value) {
    email = value;
}

// Password accessor
string Client::getPasswd() const {
    return passwd;
}

// Password mutator
void Client::setPasswd(const string& value) {
    passwd = value;
}

// The method returns true if the invoking object's password matches
// the password given in the parameter, false if otherwise
bool Client::verifyPasswd(string passwd) {
    return this->passwd == passwd;
}

// The method reads a Client object from an input stream
istream& operator>> (istream& stream, Client& c) {
    string line;

    getline(stream, line);
    c.setFname(line);

    getline(stream, line);
    c.setLname(line);

    getline(stream, line);
    c.setEmail(line);

    getline(stream, line);
    c.setPasswd(line);

    return stream;
}

/**
 * Returns a begin iterator for the offerings set
 */
set<int>::iterator Client::beginOfferings() {
    return offerings.begin();
}

/**
 * Returns an end iterator for the offerings set
 */
set<int>::iterator Client::endOfferings() {
    return offerings.end();
}

/**
 * Returns a begin iterator for the bids set
 */
set<int>::iterator Client::beginBids() {
    return bids.begin();
}

/**
 * Returns an end iterator for the bids set
 */
set<int>::iterator Client::endBids() {
    return bids.end();
}

/**
 * Adds an advertisement number to the bids set
 */
void Client::addBid(int item) {
    bids.insert(item);
}

/**
 * Adds an advertisement number to the offerings set
 */
void Client::addOffering(int item) {
    offerings.insert(item);
}
