// Working group: 2.2.2 (A.Solovev, D.Tochilkin, I.Trofimov)
// Authors: A.Solovev, D.Tochilkin and I.Trofimov
// Group: 272(2)
// Date: 8 Mar 2014

#include "Category.h"

// Trim spaces
string trimSpaces(string s) {
    return s.erase(s.find_last_not_of(" \t") + 1)
           .erase(0, s.erase(s.find_last_not_of(" \t") + 1)
                  .find_first_not_of(" \t"));
}

// The constructor initializes the private data members to default values
Category::Category(void) : number(0), parent(0), name("") {}

// The constructor accepts parameters for parent id and name
Category::Category(int parent, string name) :
    number(0), parent(parent), name(name) {}

// Accessors and mutators for number, parent and name fields
int Category::getNumber() const {
    return number;
}

void Category::setNumber(int number) {
    this->number = number;
}

int Category::getParent() const {
    return parent;
}

void Category::setParent(int parent) {
    this->parent = parent;
}

string Category::getName() const {
    return name;
}

void Category::setName(string name) {
    this->name = name;
}

// The function returns a begin iterator for the items set
set<int>::iterator Category::itemsBegin() {
    return items.begin();
}

// The function returns an end iterator for the items set
set<int>::iterator Category::itemsEnd() {
    return items.end();
}

// The function returns a begin iterator for the sub_categories set
set<Category*>::iterator Category::subCategoriesBegin() {
    return sub_categories.begin();
}

// The function returns an end iterator for the sub_categories set
set<Category*>::iterator Category::subCategoriesEnd() {
    return sub_categories.end();
}

// The method adds an advertisement number to the items set
void Category::addItem(int item) {
    items.insert(item);
}

// The method adds a category to the sub_categories set
void Category::addSubCategory(Category* value) {
    sub_categories.insert(value);
}

// Compares the invoking Category object and the parameter
bool Category::operator==(const Category& rhs) {
    return rhs.getNumber() == number;
}

// Reads a Category object from an input stream
istream& operator>>(istream& stream, Category& c) {
    string line;

    getline(stream, line);
    c.setParent(stoi(line));

    getline(stream, line);
    c.setName(trimSpaces(line));

    return stream;
}

// Fills the Listing matches with the advertisements
// that exist in the invoking category
void Category::findOfferings(Listing::iterator start,
                             Listing::iterator finish,
                             Listing& matches) {
    for (Listing::iterator iter = start; iter != finish; iter++) {
        set<int>::iterator offerings;
        offerings = find(itemsBegin(), itemsEnd(), (*iter)->getNumber());

        if (offerings != itemsEnd()) {
            matches.add(*iter);
        }
    }
}

// Fills the Listing matches with the advertisements
// that exist in the invoking category and all of its sub-categories
void Category::findOfferingsRecursive(Listing::iterator start,
                                      Listing::iterator finish,
                                      Listing& matches) {
    findOfferings(start, finish, matches);

    set<Category*>::iterator inner_iter = subCategoriesBegin();

    for (; inner_iter != subCategoriesEnd(); inner_iter++) {
        (*inner_iter)->findOfferingsRecursive(start, finish, matches);
    }
}
