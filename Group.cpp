// Working group: 2.2.2 (A.Solovev, D.Tochilkin, I.Trofimov)
// Author: Ilya Trofimov
// Group: 272(2)
// Date: 13 Feb 2014

#include "Group.h"

// The method returns the Client pointer whose object's email equals the parameter email.
Client* Group::operator[](const string& email) {
    Group::iterator it = objects.find(email);
    return it == objects.end() ? NULL : it->second;
}

// The method adds the Client pointer given by the parameter to the map objects.
void Group::add(Client* ptr) {
    if (ptr != NULL) {
        objects.insert(pair<string, Client*>(ptr->getEmail(), ptr));
    }
}

// The method returns an iterator to the first Client* in map objects.
Group::iterator Group::begin() {
    return objects.begin();
}

// The method returns an iterator to the last Client* in map objects.
Group::iterator Group::end() {
    return objects.end();
}