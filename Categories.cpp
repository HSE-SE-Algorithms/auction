// Working group: 2.2.2 (A.Solovev, D.Tochilkin, I.Trofimov)
// Author: Ilya Trofimov
// Group: 272(2)
// Date: 28 Feb 2014

#include "Categories.h"

const int Categories::TOP_LEVEL = 0;
const int Categories::NO_PARENT = 0;

// The method returns a pointer to a Category object whose
// identification number matches the parameter.
Category* Categories::operator[](const int& number) {
    Categories::iterator it = objects.find(number);
    return it == objects.end() ? NULL : it->second;
}

// The method adds a Category pointer to the objects map.
void Categories::add(Category* ptr) {
    if (ptr != NULL) {
        objects.insert(pair<int, Category*>(ptr->getNumber(), ptr));
    }
}

// The method returns an iterator to the first entry in the objects map
Categories::iterator Categories::begin() {
    return objects.begin();
}

// The method returns one position past the last entry in the objects map
Categories::iterator Categories::end() {
    return objects.end();
}